package pl.xqgsoftware.games;

import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.Transforms;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.FaceDetector;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.math.geometry.shape.Rectangle;
import org.openimaj.video.VideoDisplay;
import org.openimaj.video.VideoDisplayListener;
import org.openimaj.video.capture.VideoCaptureException;
import pl.xqgsoftware.games.realkanoid.GameBoard;
import pl.xqgsoftware.camera.VideoCaptureExtended;

import javax.swing.*;
import java.util.List;

public class RealkanoidGame {

    private final int ySkipForFaceDetection;
    private final FaceDetector<DetectedFace, FImage> faceDetector;
    private final GameBoard gameBoard;
    private final VideoDisplay<MBFImage> display;
    private final VideoCaptureExtended.ScreenResolution defaultResolution;

    public RealkanoidGame(JFrame gameFrame, VideoCaptureExtended.ScreenResolution defaultResolution) throws VideoCaptureException {
        this.defaultResolution = defaultResolution;
        this.ySkipForFaceDetection = defaultResolution.defaulHeight / 2; //detect only on bottom part of screen
        this.faceDetector = new HaarCascadeDetector(142);
        this.gameBoard = new GameBoard(defaultResolution);

        VideoCaptureExtended capturedVideo = new VideoCaptureExtended(defaultResolution);
        this.display = VideoDisplay.createVideoDisplay(capturedVideo, gameFrame);
    }

    public void doGame() throws VideoCaptureException {

        display.addVideoListener(
                new VideoDisplayListener<MBFImage>() {
                    public void beforeUpdate(MBFImage frame) {
                        frame.flipX();
                        gameBoard.setPaddlePosition(detectFace(frame));
                        gameBoard.draw(frame);

                    }

                    public void afterUpdate(VideoDisplay<MBFImage> display) {
                    }
                });
    }

    private Rectangle detectFace(MBFImage frame) {
        MBFImage partOfFrameForDetection = frame.extractROI(0, ySkipForFaceDetection, defaultResolution.defaultWidth, defaultResolution.defaulHeight - ySkipForFaceDetection);

        List<DetectedFace> faces = faceDetector.detectFaces(Transforms.calculateIntensity(partOfFrameForDetection));
        for (DetectedFace face : faces) {
            Rectangle faceBounds = face.getBounds();
            faceBounds.setBounds(faceBounds.x, faceBounds.y + ySkipForFaceDetection, faceBounds.width, faceBounds.height);
            return faceBounds;
        }
        return null;
    }

}
