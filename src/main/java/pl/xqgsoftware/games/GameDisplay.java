package pl.xqgsoftware.games;

import lombok.Data;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.video.capture.VideoCaptureException;
import pl.xqgsoftware.camera.VideoCaptureExtended;

import javax.swing.*;
import java.awt.*;

@Data
public class GameDisplay  {

    private final JFrame mainFrame;
    private final RealkanoidGame realkanoid;

    public GameDisplay() throws HeadlessException, VideoCaptureException {
        mainFrame = DisplayUtilities.createNamedWindow("realkanoid", "Realkanoid - DIY kinect clone", true);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //TODO: currently there is only one game, but in the future it will be more
        realkanoid = new RealkanoidGame(mainFrame, VideoCaptureExtended.ScreenResolution.FULL_HD);
    }

    public void showGame() throws VideoCaptureException {
        realkanoid.doGame();
    }
}
