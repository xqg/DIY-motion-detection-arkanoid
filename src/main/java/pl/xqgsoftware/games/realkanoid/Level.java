package pl.xqgsoftware.games.realkanoid;

import org.openimaj.image.MBFImage;
import org.openimaj.math.geometry.shape.Rectangle;


public class Level {

    private static final int marginX = 90;
    private static final int marginY = 80;

    private final Brick[] bricks;
    private final int maxRows;
    private final int maxColumns;

    public Level(int maxColumns, int availableWidth, int availableHeight, int[] levelDefinition) {

        this.maxColumns = maxColumns;
        this.maxRows = levelDefinition.length / maxColumns;

        this.bricks = prepareBricks(availableWidth, availableHeight, levelDefinition);
    }

    private Brick[] prepareBricks( int availableWidth, int availableHeight, int[] levelDefinition) {
        int brickWithd = (availableWidth - maxColumns * marginX) / (maxColumns);
        int brickHeight = availableHeight / (maxRows)-marginY;


        Brick[] bricks = new Brick[levelDefinition.length];

        for (int row = 1; row <= maxRows; row++) {
            for (int column = 1; column <= maxColumns; column++) {
                int x = (marginX + brickWithd) * (column - 1) + marginX;
                int y = (marginY + brickHeight) * (row -1) + marginY;

                int brickType = levelDefinition[(row - 1)*maxColumns + (column - 1)];

                bricks[(row-1)*maxColumns+(column-1)]=new Brick(x,y,brickWithd, brickHeight, brickType);
            }
        }

        return bricks;
    }

    public void draw(MBFImage frame) {
        for (Brick brick:bricks) {
            brick.draw(frame);
        }
    }

    public HitResult checkBallCollisions(Rectangle ballRectangle) {
        for (Brick brick:bricks) {
            HitResult hitResult = brick.checkCollison(ballRectangle);
            if (hitResult.hitted) {
                return hitResult;
            }
        }
        return new HitResult();
    }

    public boolean allBricksDestroyed() {
        for (Brick brick:bricks) {
            if (brick.isVisible()) {
                return false;
            }
        }
        return true;
    }
}
