package pl.xqgsoftware.games.realkanoid;

import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.typography.hershey.HersheyFont;
import org.openimaj.math.geometry.shape.Circle;
import org.openimaj.math.geometry.shape.Rectangle;
import pl.xqgsoftware.camera.VideoCaptureExtended;
import pl.xqgsoftware.games.realkanoid.sounds.RealkanoidSounds;
import pl.xqgsoftware.sounds.AudioPlayers;

import java.util.*;

public class GameBoard {

    final private int maxWidth;
    final private int maxHeight;

    //TODO: move to constructor
    final int borders = 10;
    final int ballRadius = 20;

    private int balX;
    private int balY;
    private int ballSpeedX;
    private int ballSpeedY;

    private List<Level> levels;
    private List<Bonus> bonuses;

    private Rectangle paddle;

    private int currentLevel = 0;
    private int lifes = 5;
    private int points = 0;
    private boolean lastHitWasBrick = false;

    private final RealkanoidSounds sounds;
    private boolean playIntroFirst = true;

    Random rand = new Random();
    private boolean thinPaddle = false;


    public GameBoard(VideoCaptureExtended.ScreenResolution screenResolution) {
        this.maxWidth = screenResolution.defaultWidth;
        this.maxHeight = screenResolution.defaulHeight;

        paddle = new Rectangle(maxWidth, maxHeight, 1, 1);
        levels = initializeLevels();
        resetBall();

        sounds = new RealkanoidSounds();
        sounds.playIntro();

        bonuses = new ArrayList<>();
    }

    private void resetBall() {
        balX = maxHeight - borders * 5;
        balY = maxWidth / 2 - 200;
        ballSpeedX = 15 + currentLevel;
        ballSpeedY = -21 - currentLevel;
    }

    private void resetWholeGame() {
        levels = initializeLevels();
        currentLevel = 0;
        resetBall();
        lifes = 5;
        points = 0;
        lastHitWasBrick = false;
        playIntroFirst = true;
        sounds.playIntro();
        bonuses.clear();
        thinPaddle = false;

    }

    private List<Level> initializeLevels() {
        int availableWidth = maxWidth - 2 * borders;
        int availableHeight = maxHeight * 1 / 3;

        List<Level> levels = new ArrayList<>();

        //TODO: level definitions from files
        levels.add(new Level(3, availableWidth, availableHeight,
                new int[]{
                        0, 1, 0,
                        1, 1, 1
                }));

        levels.add(new Level(3, availableWidth, availableHeight,
                new int[]{
                        0, 3, 0,
                        1, 3, 1
                }));

        levels.add(new Level(3, availableWidth, availableHeight,
                new int[]{
                        1, 2, 1,
                        3, 3, 3,
                }));
        levels.add(new Level(4, availableWidth, availableHeight,
                new int[]{
                        3, 0, 0, 3,
                        1, 2, 2, 1
                }));

        levels.add(new Level(6, availableWidth, availableHeight,
                new int[]{
                        1, 1, 3, 1, 3, 1,
                        0, 1, 2, 2, 1, 0,
                        3, 0, 3, 3, 0, 3

                }));
        levels.add(new Level(6, availableWidth, availableHeight,
                new int[]{
                        2, 1, 0, 2, 0, 1,
                        0, 0, 2, 0, 0, 2,
                        0, 1, 0, 2, 0, 2,
                        1, 0, 2, 0, 1, 0
                }));
        return levels;
    }


    public void draw(MBFImage frame) {
        if (playIntroFirst) {
            doIntro(frame);
            return;
        }

        moveBall();
        moveBonuses();
        updateLevel();

        drawBoard(frame);
        drawBall(frame);
        drawBonuses(frame);
        drawPaddle(frame);
        drawLevel(frame);
        drawScore(frame);

    }

    private void drawBonuses(MBFImage frame) {
        for (Bonus bonus : bonuses) {
            bonus.draw(frame);
        }
    }

    private void moveBonuses() {

        ListIterator<Bonus> iter = bonuses.listIterator();
        while (iter.hasNext()) {
            Bonus bonus = iter.next();
            bonus.move();
            boolean hasBonus = bonus.checkCollision(paddle);
            if (hasBonus) {
                applyBonus(bonus.getType());
                iter.remove();
            }
        }
    }

    private void applyBonus(Bonus.BonusType type) {
        switch (type) {
            case life:
                lifes++;
                break;
            case thinnerPaddle:
                thinPaddle = true;
                break;
            case normalPaddle:
                thinPaddle = false;
                break;
        }
    }

    private void doIntro(MBFImage frame) {
        frame.drawText("GET READY", maxWidth / 4, maxHeight / 2, HersheyFont.TIMES_BOLD, maxHeight / 10, RGBColour.ORANGE);
        if (sounds.isNotPlaying()) {
            playIntroFirst = false;
        }
    }

    private void drawScore(MBFImage frame) {
        frame.drawText("Life left - " + lifes, 10, 35, HersheyFont.TIMES_BOLD, 30, RGBColour.RGB(50, 51, 179));
        frame.drawText("Score - " + points, maxWidth - 240, 35, HersheyFont.TIMES_BOLD, 30, RGBColour.RGB(50, 51, 239));
    }

    private void updateLevel() {
        if (getCurrentLevel().allBricksDestroyed()) {
            resetBall();
            currentLevel += 1;
            sounds.playNextLevel();
        }
    }

    private void drawLevel(MBFImage frame) {
        getCurrentLevel().draw(frame);
    }

    private Level getCurrentLevel() {
        return levels.get(currentLevel);
    }

    private void drawBall(MBFImage frame) {
        frame.drawShapeFilled(new Circle(balX, balY, ballRadius), RGBColour.RGB(203, 205, 20));
    }

    private void drawBoard(MBFImage frame) {
        frame.drawShape(new Rectangle(0f, 0f, maxWidth - 4, maxHeight - 4), 4, RGBColour.BLACK);
    }

    private void drawPaddle(MBFImage image) {
        if (paddle != null) {
            image.drawShapeFilled(paddle, RGBColour.RGB(191, 20, 20));
            image.drawShape(paddle, 4, RGBColour.RGB(140, 20, 20));
        }
    }

    private void moveBall() {
        int futureX = balX + ballSpeedX;
        int futureY = balY + ballSpeedY;

        boolean xReachEndOfBoard = futureX > maxWidth - borders - ballRadius || futureX < borders;
        if (xReachEndOfBoard) {
            ballSpeedX *= -1;
            sounds.playHitWall();
            lastHitWasBrick = false;
        }

        boolean yReachBottomOfBoard = futureY > maxHeight - borders - ballRadius;
        boolean yReachEndOfBoard = yReachBottomOfBoard || futureY < borders;
        boolean yReachPaddle = paddle.isOverlapping(new Rectangle(futureX, futureY, ballRadius, ballRadius));

        if (yReachEndOfBoard || yReachPaddle) {
            ballSpeedY *= -1;
            lastHitWasBrick = false;
        }

        if (yReachBottomOfBoard) {
            lifes--;
            sounds.playLoseLife();
            lastHitWasBrick = false;

            if (lifes <= 0) {
                sounds.playGameOver();

                resetWholeGame();
            }
        }

        if (yReachPaddle) {
            changeBallSpeedRandomly();
            if (sounds.isNotPlaying()) {
                sounds.playHitPaddle();
            }
            lastHitWasBrick = false;
        }

        HitResult hitResult = getCurrentLevel().checkBallCollisions(new Rectangle(futureX, futureY, ballRadius, ballRadius));
        if (hitResult.hitted) {
            ballSpeedY *= -1;
            sounds.playBallHitNormalBrick();
            points++;

            if (lastHitWasBrick) {
                points++;   //additional point if before was also a brick
            }

            lastHitWasBrick = true;

            if (hitResult.bonus != null) {
                bonuses.add(hitResult.bonus);
            }
        }

        balX += ballSpeedX;
        balY += ballSpeedY;
    }

    private void changeBallSpeedRandomly() {
        int bound = 100 - 13 * currentLevel;     //less possibilities on higher levels
        int randomNumber = rand.nextInt(bound) + 1;

        if (randomNumber < 8) {
            int direction = rand.nextInt(3) - 1;
            ballSpeedX += direction;
        }
    }

    public void setPaddlePosition(Rectangle paddle) {
        if (paddle == null || paddle.height <= 2 || paddle.width < 2) {
            //no update of paddle if it can not be detected
            return;
        }

        this.paddle = paddle;
        if (thinPaddle) {
            paddle.width = paddle.width / 2;
        }
    }
}
