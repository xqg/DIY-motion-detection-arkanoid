package pl.xqgsoftware.games.realkanoid.sounds;

import pl.xqgsoftware.sounds.AudioPlayers;

public class RealkanoidSounds extends AudioPlayers {

    private final String hitPaddle = "paletka.mp3";
    private final String hitBrick = "jajo.mp3";
    private final String loseLive = "uuu.mp3";
    private final String gameOver = "gameOver.mp3";
    private final String hitWall = "sciana.mp3";
    private final String nextLevel = "hyhy.mp3";
    private final String intro = "hihiRys.mp3";

    public void playBallHitNormalBrick() {
        play(hitBrick);
    }

    public void playHitPaddle() {
        play(hitPaddle);
    }

    public void playLoseLife() {
        play(loseLive);
    }

    public void playHitWall() {
        play(hitWall);
    }

    public void playGameOver() {
        play(gameOver, true);
    }

    public void playNextLevel() {
        play(nextLevel, true);
    }

    public void playIntro() {
        play(intro);
    }

}
