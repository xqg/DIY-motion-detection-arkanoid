package pl.xqgsoftware.games.realkanoid;

import lombok.Data;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.math.geometry.shape.Rectangle;

import java.util.Random;

@Data
public class Brick {

    private final int x;
    private final int y;
    private final int width;
    private final int height;
    private int type;

    private final Rectangle brickRectangle;
    Random rand = new Random();

    public Brick(int x, int y, int width, int height, int type) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.type = type;
        this.brickRectangle = new Rectangle(x, y, width, height);

    }

    public void draw(MBFImage frame) {
        if (!isVisible()) {
            return;
        }

        frame.drawShapeFilled(brickRectangle, getColor());
        frame.drawShape(brickRectangle, 4, RGBColour.RGB(0, 9, 80));
    }

    public HitResult checkCollison(Rectangle rectangle) {
        HitResult hitResult = new HitResult();

        boolean isHitted = isVisible() && brickRectangle.isOverlapping(rectangle);
        if (isHitted) {
            hitResult.bonus = getBonus();
            this.type -= 1;
        }

        hitResult.hitted = isHitted;

        return hitResult;
    }

    public boolean isVisible() {
        return type != 0;
    }

    private Bonus getBonus() {
        switch (type) {
            case 3:
                int random = rand.nextInt(16)+1;
                if (random <=6) {
                    return createBonus(Bonus.BonusType.life);
                }
                if (random<=11) {
                    return createBonus(Bonus.BonusType.normalPaddle);
                }
                return createBonus(Bonus.BonusType.thinnerPaddle);
            default:
                return null;
        }
    }

    private Bonus createBonus(Bonus.BonusType bonusType) {
        return new Bonus(x+width/2, y+height, 1920, bonusType); //TODO: get rid of 1920 hardcoded

    }

    private Float[] getColor() {
        switch (type) {
            case 3:
                return RGBColour.RGB(122, 85, 200);
            case 2:
                return RGBColour.RGB(71, 87, 208);
            default:
                return RGBColour.RGB(0, 9, 193);
        }
    }
}
