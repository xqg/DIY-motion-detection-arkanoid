package pl.xqgsoftware.games.realkanoid;

import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.math.geometry.shape.Ellipse;
import org.openimaj.math.geometry.shape.Rectangle;

public class Bonus {

    public enum BonusType {life, normalPaddle, thinnerPaddle}

    private final int maxY;
    private final int width = 10;
    private final int height = 18;
    private final BonusType type;
    private float rotation = 0;
    private float rotationDelta = 0.2f;
    int speedy = 6;

    int x;
    int y;


    public Bonus(int x, int y, int maxY, BonusType type) {
        this.x = x;
        this.y = y;
        this.maxY = maxY;

        this.type = type;
    }

    public BonusType getType() {
        return type;
    }

    public void move() {
        if (isVisible()) {
            y += speedy;
            rotation += rotationDelta;
            if (rotation > 360) {
                rotation -= 360;
            }
        }
    }

    public void draw(MBFImage frame) {
        if (!isVisible()) {
            return;
        }

        Ellipse bonusRectangle = new Ellipse(x, y, width, height, rotation);
        frame.drawShapeFilled(bonusRectangle, getColor());
        frame.drawShape(bonusRectangle, 2, RGBColour.RGB(60, 9, 80));
    }

    public boolean checkCollision(Rectangle rectangle) {
        Rectangle bonusRectangle = new Rectangle(x, y, width, height);
        boolean isHitted = isVisible() && bonusRectangle.isOverlapping(rectangle);
        return isHitted;
    }

    public boolean isVisible() {
        return y < maxY;
    }

    private Float[] getColor() {
        switch (type) {
            case life:
                return RGBColour.RGB(255, 0, 0);
            case thinnerPaddle:
                return RGBColour.RGB(120, 190, 120);
            case normalPaddle:
            default:
                return RGBColour.RGB(0, 180, 200);
        }
    }
}
