package pl.xqgsoftware.camera;

import org.openimaj.video.capture.VideoCapture;
import org.openimaj.video.capture.VideoCaptureException;

public class VideoCaptureExtended extends VideoCapture {

    public enum ScreenResolution {
        FULL_HD(1920,1080),
        HD(1280,720),
        MEDIUM(540,480),
        POOR(320,200);

        public int defaultWidth= 1280;
        public int defaulHeight=720;


        ScreenResolution(int width, int height) {
            this.defaultWidth=width;
            this.defaulHeight=height;
        }

    }

    public VideoCaptureExtended(ScreenResolution screenResolution) throws VideoCaptureException {
        super(screenResolution.defaultWidth, screenResolution.defaulHeight);
    }

}
