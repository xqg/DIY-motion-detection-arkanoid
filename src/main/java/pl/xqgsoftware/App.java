package pl.xqgsoftware;

import org.openimaj.video.capture.VideoCaptureException;
import pl.xqgsoftware.games.GameDisplay;
import pl.xqgsoftware.games.RealkanoidGame;

public class App {

    public static void main(String[] args) throws VideoCaptureException {

        GameDisplay gameDisplay = new GameDisplay();
        gameDisplay.showGame();

    }


}
