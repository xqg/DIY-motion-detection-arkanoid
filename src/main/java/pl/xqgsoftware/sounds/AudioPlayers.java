package pl.xqgsoftware.sounds;

import javazoom.jl.decoder.JavaLayerException;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;

public class AudioPlayers implements Runnable {

    private javazoom.jl.player.Player player;

    protected void play(String filename) {
        play(filename, false);
    }

    protected void play(String filename, boolean synchronous) {
        try {
            ClassPathResource resource = new ClassPathResource(filename);
            InputStream speechStream = resource.getInputStream();

            player = new javazoom.jl.player.Player(speechStream, javazoom.jl.player.FactoryRegistry.systemRegistry().createAudioDevice());
            if (synchronous) {
                player.play();
            } else {
                createPlayerThread().start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Thread createPlayerThread() {
        return new Thread(this, "Audio player thread");
    }

    @Override
    public void run() {
        if (this.player != null) {
            try {
                this.player.play();
            } catch (JavaLayerException var2) {
                System.err.println("Problem playing audio: " + var2);
            }
        }

    }
    public boolean isNotPlaying() {
        return player.isComplete();
    }
}
